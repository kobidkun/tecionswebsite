let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */




mix.styles([
    'resources/assets/files/assets/css/bulma.css',
    'resources/assets/files/assets/css/core_green.css',
    'resources/assets/files/assets/js/slick-carousel/slick.css',
    'resources/assets/files/assets/js/slick-carousel/slick-theme.css',
    'resources/assets/files/assets/js/ggpopover/ggtooltip.css',
    'resources/assets/files/assets/css/icons.min.css'
], 'public/css/main.min.css');

mix.scripts([
    'resources/assets/files/assets/js/core/jquery.min.js',
    'resources/assets/files/assets/js/core/modernizr.min.js',
    'resources/assets/files/assets/js/slick-carousel/slick.min.js',
    'resources/assets/files/assets/js/ggpopover/ggtooltip.js',
    'resources/assets/files/assets/js/scrollreveal/scrollreveal.min.js',
    'resources/assets/files/assets/js/main.js',
    'resources/assets/files/assets/js/pages/landingv1.js',

], 'public/js/main.min.js');
