<?php

namespace App\Http\Controllers;

use App\Http\Requests\QueryValidation;
use App\Query;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Querymain;
class QueryController extends Controller
{
    public function Query(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'city' => 'required',
        ]);

        $query = new Query();
        $query->name = $request->name;
        $query->email = $request->email;
        $query->mobile = $request->mobile;
        $query->city = $request->city;
        $query->description = $request->description;
        $query->save();


        Mail::to('info@tecions.com')->send(new Querymain($query));



        return redirect('/');
    }
}
