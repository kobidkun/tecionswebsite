
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msvalidate.01" content="3DCECFDE85EB6C9DE7C5B6E7FE328F27" />


    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#e94754">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#e94754">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#e94754">

    <link rel="icon" type="image/png" href="{{asset('assets/images/favicon.png')}}" />
    <!-- Core css -->
    <link rel="stylesheet" id="bulma" href="{{asset('/css/main.min.css')}}" />



</head>

<body>
<!-- Pageloader -->

<div class="hero is-theme-primary">
    <div class="navbar-wrapper navbar-fade navbar-light">
        <div class="hero-head">

            <!-- Nav -->
            <div class="container">
                <nav class="nav">
                    <div class="container big">
                        <div class="nav-left">
                            <a class="nav-item" href="/">
                                <img class="light-logo" src="{{asset('/img/logo-light.png')}}" alt="">
                            </a>
                            <!-- Sidebar trigger -->
                            {{-- <a id="navigation-trigger" class="nav-item hamburger-btn" href="javascript:void(0);">
                                         <span class="menu-toggle">
                                             <span class="icon-box-toggle">
                                                 <span class="rotate">
                                                     <i class="icon-line-top"></i>
                                                     <i class="icon-line-center"></i>
                                                     <i class="icon-line-bottom"></i>
                                                 </span>
                                             </span>
                                         </span>
                             </a>--}}
                            <a href="/#product" class="nav-item is-tab nav-inner is-not-mobile">
                                Core Services
                            </a>

                        </div>
                        <!-- Responsive toggle -->
                        <span class="nav-toggle">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </span>
                        <div class="nav-right nav-menu">

                            <a href="#product" class="nav-item is-tab nav-inner is-menu-mobile">
                                Core Services
                            </a>
                            {{-- <a href="landing-v1-pricing.html" class="nav-item is-tab nav-inner is-menu-mobile">
                                 Pricing
                             </a>
                             <a href="landing-v1-login.html" class="nav-item is-tab nav-inner is-menu-mobile">
                                 Login
                             </a>--}}
                            <span class="nav-item">
                                        <a id="signup-btn" href="tel:+917001304853"
                                           class="button button-signup btn-outlined is-bold btn-align light-btn">
                                            +91 7001304853
                                        </a>
                                    </span>
                        </div>
                    </div>
                </nav>
            </div>
            <!-- /Nav -->

        </div>
    </div>

@yield('content')

{{--<!-- Footer -->
<footer class="footer footer-light-left">
    <div class="container">
        <div class="columns is-vcentered">
            <!-- Column -->
            <div class="column is-6">
                <div class="mb-20">
                    <img class="small-footer-logo" src="{{asset('img/logo-dark-sml.png')}}" alt="">
                </div>
                <div>
                    <span class="moto">Designed  with <i class="fa fa-heart color-red"></i> by Tecions
                    <br>(c) 2018 Kunda sales Pvt Ltd.
                    </span>
                    <nav class="level is-mobile mt-20">
                        <div class="level-left level-social">
                            <a href="https://www.facebook.com/Tecionsofficial/" target="_blank"  class="level-item">
                                <span class="icon"><i class="fa fa-facebook"></i></span>
                            </a>
                            <a class="level-item">
                                <span class="icon"><i class="fa fa-twitter"></i></span>
                            </a>
                            </a>
                            <a href="https://github.com/tecions" target="_blank" class="level-item">
                                <span class="icon"><i class="fa fa-github"></i></span>
                            </a>
                        </div>
                    </nav>

                </div>
            </div>
            <!-- Column -->
            <div class="column">
                <div class="footer-nav-right">
                    <a class="footer-nav-link" href="mailto:info@tecions.com" target="_blank">info@tecions.com</a>
                    <a class="footer-nav-link" href="tel:7001304853" target="_blank">+917001304853</a>

                </div>





                <div class="footer-nav-right">
                    <a class="footer-nav-link" href="/">Home</a>
                    <a class="footer-nav-link" href="#product">Services</a>
                    <a class="footer-nav-link" href="landing-v1-features.html">T&Cs</a>
                    <a class="footer-nav-link" href="landing-v1-pricing.html">Privacy</a>
                </div>

                <div class="footer-nav-right">
                    <a class="footer-nav-link" href="https://www.google.co.in/maps/place/Tecions/@26.690428,88.4204274,19.95z/data=!4m12!1m6!3m5!1s0x39e443dfd2a79fed:0x426dfd0232cd749b!2sTecions!8m2!3d26.690381!4d88.420616!3m4!1s0x39e443dfd2a79fed:0x426dfd0232cd749b!8m2!3d26.690381!4d88.420616" target="_blank">Shiv shakti Kali Bari road, Sukantapally, Siliguri 734005</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /Footer -->--}}



<footer class="footer footer-light">
    <div class="container">
        <div class="columns">
            <div class="column">
                <div class="footer-column">
                    <div class="footer-header">
                        <h3>Services</h3>
                    </div>
                    <ul class="link-list">
                        <li><a href="/#product">Application Development</a></li>
                        <li><a href="/#product">Mobility</a></li>
                        <li><a href="/#product">Cloud Computing</a></li>
                        <li><a href="/#product">Website Development</a></li>
                        <li><a href="/#product">Softwere Development</a></li>
                        <li><a href="/#product">Online Ads</a></li>
                    </ul>
                </div>
            </div>
            <div class="column">
                <div class="footer-column">
                    <div class="footer-header">
                        <h3>Company</h3>
                    </div>
                    <ul class="link-list">
                        <li><a href="/terms-conditions">Terms of service</a></li>
                        <li><a href="/refund-policy">Refund</a></li>
                        <li><a href="privacy-policy">Privacy policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="column">
                <div class="footer-column">
                    <div class="footer-header">
                        <h3>Reach us </h3>
                    </div>
                    <span class="moto dark-text">
                        <a class="footer-nav-link" href="mailto:info@tecions.com" target="_blank">	&nbsp; &nbsp;info@tecions.com</a>

                        <br>

                    <a class="footer-nav-link" href="tel:7001304853" target="_blank">	&nbsp; &nbsp;+917001304853</a>

<br>
<br>

                        <i class="fa fa-map-marker color-red"></i> Shiv Shakti kali bari Road. <br> 		&nbsp; &nbsp;Sukantapally Siliguri
                    <br> &nbsp;	&nbsp;P.O Siliguri Bazaar, <br>	&nbsp; &nbsp;Siliguri 734005, India
                    </span>

                </div>
            </div>
            <div class="column">
                <div class="footer-column">
                    <div class="footer-logo">
                        <img src="{{asset('assets/images/logos/bulkit-logo-green.png')}}" alt="">
                    </div>
                    <div class="footer-header">
                        <nav class="level is-mobile">
                            <div class="level-left level-social">
                                <a href="https://www.facebook.com/Tecionsofficial/" target="_blank"  class="level-item">
                                    <span class="icon"><i class="fa fa-facebook"></i></span>
                                </a>
                                <a class="level-item">
                                    <span class="icon"><i class="fa fa-twitter"></i></span>
                                </a>
                                </a>
                                <a href="https://github.com/tecions" target="_blank" class="level-item">
                                    <span class="icon"><i class="fa fa-github"></i></span>
                                </a>
                            </div>
                        </nav>
                    </div>
                    <div class="copyright">
                        <span class="moto dark-text">&copy; 2018 Kunda sales Pvt Ltd. <br> Designed with
                            <i class="fa fa-heart color-red"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /Light footer -->





{{--<!-- Side navigation -->
<div class="side-navigation-menu">
    <!-- Categories menu -->
    <div class="category-menu-wrapper">
        <!-- Menu -->
        <ul class="categories">
            <li class="square-logo"><img src="assets/images/logos/square-white.svg" alt=""></li>
            <li class="category-link is-active" data-navigation-menu="demo-pages"><i class="sl sl-icon-layers"></i></li>
            <li class="category-link" data-navigation-menu="components"><i class="sl sl-icon-grid"></i></li>
            <li class="category-link" data-navigation-menu="extras"><i class="sl sl-icon-present"></i></li>
        </ul>
        <!-- Menu -->

        <ul class="author">
            <li>
                <!-- Theme author -->
                <a href="javascript:void(0);">
                    <img class="main-menu-author" src="assets/images/logos/cssninja.svg" alt="">
                </a>
            </li>
        </ul>
    </div>
    <!-- /Categories menu -->

    <!-- Navigation menu -->
    <div id="demo-pages" class="navigation-menu-wrapper animated preFadeInRight fadeInRight">
        <!-- Navigation Header -->
        <div class="navigation-menu-header">
            <span>Demo pages</span>
            <a class="ml-auto hamburger-btn navigation-close" href="javascript:void(0);">
                        <span class="menu-toggle">
                            <span class="icon-box-toggle">
                                <span class="rotate">
                                    <i class="icon-line-top"></i>
                                    <i class="icon-line-center"></i>
                                    <i class="icon-line-bottom"></i>
                                </span>
                            </span>
                        </span>
            </a>
        </div>
        <!-- Navigation Body -->
        <ul class="navigation-menu">
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">weekend</span>Agency kit</a>
                <ul>
                    <li><a class="is-submenu" href="agency.html">Homepage</a></li>
                    <li><a class="is-submenu" href="agency-about.html">About</a></li>
                    <li><a class="is-submenu" href="agency-portfolio.html">Portfolio</a></li>
                    <li><a class="is-submenu" href="agency-contact.html">Contact</a></li>
                    <li><a class="is-submenu" href="agency-blog.html">Blog</a></li>
                    <li><a class="is-submenu" href="agency-post-sidebar.html">Post sidebar</a></li>
                    <li><a class="is-submenu" href="agency-post-nosidebar.html">Post no sidebar</a></li>
                </ul>
            </li>
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">wb_incandescent</span>Startup kit</a>
                <ul>
                    <li><a class="is-submenu" href="startup.html">Homepage</a></li>
                    <li><a class="is-submenu" href="startup-about.html">About</a></li>
                    <li><a class="is-submenu" href="startup-product.html">Product</a></li>
                    <li><a class="is-submenu" href="startup-contact.html">Contact</a></li>
                    <li><a class="is-submenu" href="startup-login.html">Login</a></li>
                    <li><a class="is-submenu" href="startup-signup.html">Sign up</a></li>
                </ul>
            </li>
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">looks</span>Landing kit v1</a>
                <ul>
                    <li><a class="is-submenu" href="landing.html">Landing page</a></li>
                    <li><a class="is-submenu" href="landing-features.html">Feature page</a></li>
                    <li><a class="is-submenu" href="landing-pricing.html">Pricing page</a></li>
                    <li><a class="is-submenu" href="landing-login.html">Login page</a></li>
                    <li><a class="is-submenu" href="landing-signup.html">Signup page</a></li>
                </ul>
            </li>
            <li class="has-children active-section"><a class="parent-link" href="#"><span class="material-icons">invert_colors</span>Landing kit v2</a>
                <ul>
                    <li><a class="is-submenu" href="landing-v1.html">Landing page</a></li>
                    <li><a class="is-submenu" href="landing-v1-features.html">Feature page</a></li>
                    <li><a class="is-submenu" href="landing-v1-pricing.html">Pricing page</a></li>
                    <li><a class="is-submenu" href="landing-v1-login.html">Login page</a></li>
                    <li><a class="is-submenu" href="landing-v1-signup.html">Signup page</a></li>
                </ul>
            </li>
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">chat_bubble</span>Landing kit v3</a>
                <ul>
                    <li><a class="is-submenu" href="landing-v2.html">Landing page</a></li>
                    <li><a class="is-submenu" href="landing-v2-features.html">Feature page</a></li>
                    <li><a class="is-submenu" href="landing-v2-pricing.html">Pricing page</a></li>
                    <li><a class="is-submenu" href="landing-v2-login.html">Login page</a></li>
                    <li><a class="is-submenu" href="landing-v2-signup.html">Signup page</a></li>
                </ul>
            </li>
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">business</span>Landing kit v4</a>
                <ul>
                    <li><a class="is-submenu" href="landing-v3.html">Landing page</a></li>
                    <li><a class="is-submenu" href="landing-v3-pricing.html">Pricing page</a></li>
                    <li><a class="is-submenu" href="landing-v3-help.html">Help center</a></li>
                    <li><a class="is-submenu" href="landing-v3-help-category.html">Help category</a></li>
                    <li><a class="is-submenu" href="landing-v3-help-article.html">Help article</a></li>
                    <li><a class="is-submenu" href="landing-v3-signup.html">Login</a></li>
                    <li><a class="is-submenu" href="landing-v3-login.html">Sign up</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /Navigation menu -->

    <!-- Navigation menu -->
    <div id="components" class="navigation-menu-wrapper animated preFadeInRight fadeInRight is-hidden">
        <!-- Navigation Header -->
        <div class="navigation-menu-header">
            <span>Components</span>
            <a class="ml-auto hamburger-btn navigation-close" href="javascript:void(0);">
                        <span class="menu-toggle">
                            <span class="icon-box-toggle">
                                <span class="rotate">
                                    <i class="icon-line-top"></i>
                                    <i class="icon-line-center"></i>
                                    <i class="icon-line-bottom"></i>
                                </span>
                            </span>
                        </span>
            </a>
        </div>
        <!-- Navigation body -->
        <ul class="navigation-menu">
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">view_quilt</span>Layout</a>
                <ul>
                    <li><a class="is-submenu" href="_components-layout-grid.html">Grid system</a></li>
                    <li><a class="is-submenu" href="_components-layout-navbars.html">Navbars</a></li>
                    <li><a class="is-submenu" href="_components-layout-video.html">Video background</a></li>
                    <li><a class="is-submenu" href="_components-layout-parallax.html">Parallax</a></li>
                    <li><a class="is-submenu" href="_components-layout-headers.html">Headers</a></li>
                    <li><a class="is-submenu" href="_components-layout-footers.html">Footers</a></li>
                    <li><a class="is-submenu" href="_components-layout-typography.html">Typography</a></li>
                    <li><a class="is-submenu" href="_components-layout-colors.html">Colors</a></li>
                </ul>
            </li>
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">view_stream</span>Sections</a>
                <ul>
                    <li><a class="is-submenu" href="_components-sections-features.html">Features</a></li>
                    <li><a class="is-submenu" href="_components-sections-pricing.html">Pricing</a></li>
                    <li><a class="is-submenu" href="_components-sections-team.html">Team</a></li>
                    <li><a class="is-submenu" href="_components-sections-testimonials.html">Testimonials</a></li>
                    <li><a class="is-submenu" href="_components-sections-clients.html">Clients</a></li>
                    <li><a class="is-submenu" href="_components-sections-counters.html">Counters</a></li>
                    <li><a class="is-submenu" href="_components-sections-carousel.html">Carousel</a></li>
                </ul>
            </li>
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">playlist_add_check</span>Basic UI</a>
                <ul>
                    <li><a class="is-submenu" href="_components-basicui-cards.html">Cards</a></li>
                    <li><a class="is-submenu" href="_components-basicui-buttons.html">Buttons</a></li>
                    <li><a class="is-submenu" href="_components-basicui-dropdowns.html">Dropdowns</a></li>
                    <li><a class="is-submenu" href="_components-basicui-lists.html">Lists</a></li>
                    <li><a class="is-submenu" href="_components-basicui-modals.html">Modals</a></li>
                    <li><a class="is-submenu" href="_components-basicui-tabs.html">Tabs & pills</a></li>
                    <li><a class="is-submenu" href="_components-basicui-accordion.html">Accordions</a></li>
                    <li><a class="is-submenu" href="_components-basicui-badges.html">Badges & labels</a></li>
                    <li><a class="is-submenu" href="_components-basicui-popups.html">Popups</a></li>
                </ul>
            </li>
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">playlist_add</span>Advanced UI</a>
                <ul>
                    <li><a class="is-submenu" href="_components-advancedui-tables.html">Tables</a></li>
                    <li><a class="is-submenu" href="_components-advancedui-timeline.html">Timeline</a></li>
                    <li><a class="is-submenu" href="_components-advancedui-boxes.html">Boxes</a></li>
                    <li><a class="is-submenu" href="_components-advancedui-messages.html">Messages</a></li>
                </ul>
            </li>
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">check_box</span>Forms</a>
                <ul>
                    <li><a class="is-submenu" href="_components-forms-inputs.html">Inputs</a></li>
                    <li><a class="is-submenu" href="_components-forms-controls.html">Controls</a></li>
                    <li><a class="is-submenu" href="_components-forms-layouts.html">Form layouts</a></li>
                    <li><a class="is-submenu" href="_components-forms-uploader.html">Uploader</a></li>
                </ul>
            </li>
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">adjust</span>Icons</a>
                <ul>
                    <li><a class="is-submenu" href="_components-icons-im.html">Icons Mind</a></li>
                    <li><a class="is-submenu" href="_components-icons-sl.html">Simple Line Icons</a></li>
                    <li><a class="is-submenu" href="_components-icons-fa.html">Font Awesome</a></li>
                    <li><a class="is-submenu" href="https://material.io/icons/" target="_blank">Material Icons</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /Navigation menu -->

    <!-- Navigation menu -->
    <div id="extras" class="navigation-menu-wrapper animated preFadeInRight fadeInRight is-hidden">
        <!-- Navigation Header -->
        <div class="navigation-menu-header">
            <span>Extras</span>
            <a class="ml-auto hamburger-btn navigation-close" href="javascript:void(0);">
                        <span class="menu-toggle">
                            <span class="icon-box-toggle">
                                <span class="rotate">
                                    <i class="icon-line-top"></i>
                                    <i class="icon-line-center"></i>
                                    <i class="icon-line-bottom"></i>
                                </span>
                            </span>
                        </span>
            </a>
        </div>
        <!-- Navigation body -->
        <ul class="navigation-menu">
            <li class="has-children"><a class="parent-link" href="#"><span class="material-icons">dashboard</span>Dashboard kit</a>
                <ul>
                    <li><a class="is-submenu" href="dashboard.html">Main Layout</a></li>
                    <li><a class="is-submenu" href="dashboard-dark-nav.html">Dark Sidebar</a></li>
                    <li><a class="is-submenu" href="dashboard-blank.html">Blank page</a></li>
                    <li><a class="is-submenu" href="dashboard-chartjs.html">Chartjs charts</a></li>
                    <li><a class="is-submenu" href="dashboard-billboardjs.html">Billboardjs charts</a></li>
                    <li><a class="is-submenu" href="dashboard-peityjs.html">Peity charts</a></li>
                    <li><a class="is-submenu" href="dashboard-widgets-data.html">Data widgets</a></li>
                    <li><a class="is-submenu" href="dashboard-widgets-social.html">Social widgets</a></li>
                    <li><a class="is-submenu" href="dashboard-feed.html">Dashboard feed</a></li>
                    <li><a class="is-submenu" href="dashboard-feed-post.html">Feed post</a></li>
                    <li><a class="is-submenu" href="dashboard-login.html">Login</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /Navigation menu -->
</div>
<!-- /Side navigation -->--}}


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>






<!-- Core js -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
<script src="{{asset('/js/main.min.js')}}"></script>
<script src="{{asset('assets/js/pages/components-modals.js')}}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script>
        (function(e,t,n,i,s,a,c){e[n]=e[n]||function(){(e[n].q=e[n].q||[]).push(arguments)}
        ;a=t.createElement(i);c=t.getElementsByTagName(i)[0];a.async=true;a.src=s
        ;c.parentNode.insertBefore(a,c)
        })(window,document,"galite","script","https://cdn.jsdelivr.net/npm/ga-lite@2/dist/ga-lite.min.js");

        galite('create', 'UA-72693286-1', 'auto');
        galite('send', 'pageview');
    </script>


</body>

</html>