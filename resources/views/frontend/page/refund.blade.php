@extends('frontend.base')


@section('content')
    <title>Tecions - Refund Policy</title>
    <!-- Hero and nav -->


    <!-- Hero image -->
    <div id="main-hero" class="hero-body is-clean">
        <div class="container has-text-centered">
            <div class="columns is-vcentered">
                <div class="column is-5 caption-column has-text-left">
                    <h1 class="clean-title light-text">
                        Refund Policy
                    </h1>







                </div>
                <div class="column is-9 is-offset-1">

                </div>

            </div>
        </div>
    </div>
    <!-- /Hero image -->
    </div>
    <!-- /Hero and nav -->

    <section class="section is-medium">
        <div class="container">
            <!-- Title -->
            <div class="section-title-wrapper">
                <h2 class="title dark-text text-bold main-title is-2 has-text-centered">
                    Refund Policy
                </h2>

            </div>
            <!-- /Title -->
            <div class="columns">
                <div class="column is-6 is-offset-3">
                    <div class="content">

                        <p>

                            <div class="text-widget">
                                <h2>Refund Policy</h2>
                                <div class="text-widget">
                        <p>Refund policy of Tecions has been originated to find the situation under which Tecions renders a refund method for claiming a refund and Tecions's responsibility in circumstances resulting in such a claim. You are declaring by registering any of our services that you already accept and agree with all the terms and condition outline in the refund policy.<span>&nbsp;</span><br /><br />For bettering our website's content, to notify the all members about our website updates or find into the problems with clients request we used all the information. You can inform us by written through the Email and letter if you don't want to get any email in future from Tecions.</p>
                        <h4>Coverage and Scope</h4>
                        <p>Tecions managed and owned any website the covers the all by our refund policy.<span>&nbsp;</span><br /><br />Websites which are not control or managed by Tecions, the refund policy application does not for those companies including the 3rd party service and product providers leap by contract and also, any third party websites to which Tecions website link.</p>
                        <h4>Response a Complaint</h4>
                        <p>We Tecions take every client requirements with the final product in sight. As the customers it is much our responsibility. Where dissatisfaction related to services comes we believe that every effort should be made for reaching fully acceptable solutions. Refund should be considered when the things are completely out of control.<span>&nbsp;</span><br /><br />A mutually acceptable solution is the final aim of Tecions. it is our headrest request to you if you are not satisfied and think about going for refund claim then please think about it again and if you really want to refund then contact with us by written.</p>
                        <h4>Eligibility for refund</h4>
                        <h5><b>Website, Web/Graphic Design</b></h5>
                        <ul class="no-liststyle">
                            <li><strong>1. Full Refund:</strong><br />If the project is nit been started or the initial design has not been approved. It will take 180 days to refund the full amount and after the upfront payments days.</li>
                            <li><strong>2. Partial Refund:</strong><br />As per our delivery policy if there is a failure to deliver after the approval of the initial design style. The partial refund will be in proportion to the work completed.</li>
                            <li><strong>3. No refunds:</strong><br />If the project has been completed and uploaded on the server.</li>
                        </ul>
                        <h5><b>Logo Design / Brochure Design</b></h5>
                        <ul class="no-liststyle">
                            <li><strong>1. Full Refund:</strong><br />If the project is not been started or the initial design has not been approved. It will take 180 days to refund the full amount and after the upfront payments days.</li>
                            <li><strong>2. Partial Refund:</strong><br />As per our delivery policy if there is a failure to deliver after the approval of the initial design style. The partial refund will be in proportion to the work completed.</li>
                            <li><strong>3. No refunds:</strong><br />If the project has been completed and uploaded on the server.</li>
                        </ul>
                        <h5><b>Web Programming</b></h5>
                        <ul class="no-liststyle">
                            <li><strong>1. Full Refund</strong><br />If the project has not been initiated.</li>
                            <li>2. For every web programming project there is an agreement. However, if no arrangement and no clear discussion of refund policy, the following delivery project will hold true.</li>
                            <li>3. If we are not able to complete the project in according to our delivery policy and contract agreement and based on the issues as per the declared by the services provider will be decided the partial refund.</li>
                        </ul>
                        <h5><b>Dedicate Hiring</b></h5>
                        <ul class="no-liststyle">
                            <li><strong>1. Full Refund :</strong><br />If the project is not been started by the programmer/ designer/content writer/SEO expert or any other expert. It will take 180 days to refund the full amount and after the upfront payments days.</li>
                            <li><strong>2. Proportionate Refund :</strong><br />The part of the project has been done till the time when we will get written request for cancelling.</li>
                            <li><strong>3. No Refund :</strong><br />The services already been rendered and the client does not satisfied with any level of work, so the client must be informed or noticed us. Any negotiations at a later date will not be entertained.</li>
                        </ul>
                        <h5><b>Applicability of the Delivery Policy</b></h5>
                        <ul class="no-liststyle">
                            <li>1. The project is not taken to be void unless the agreed upon payments are clear.</li>
                            <li>2. If the required information for the successful completion of the project is not given to us at proper time refund policy is not applicable.</li>
                            <li>3. Tecions is not liable to follow its delivery or refund commitments. If the info provided by the customers is incomplete and/or complete information regarding the project is not provided at the initiation of the project.</li>
                            <li>4. There is no provision for compensation for the delay of delivery under any conditions, until and unless there is an agreement signed with a penalty clause for delay in delivery.</li>
                        </ul>
                    </div>
                </div>



                    </p>
                </div>


            </div>
        </div>
        </div>
    </section>



@endsection