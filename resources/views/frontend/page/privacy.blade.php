@extends('frontend.base')


@section('content')
    <title>Tecions - Privacy Policy</title>
    <!-- Hero and nav -->


    <!-- Hero image -->
    <div id="main-hero" class="hero-body is-clean">
        <div class="container has-text-centered">
            <div class="columns is-vcentered">
                <div class="column is-5 caption-column has-text-left">
                    <h1 class="clean-title light-text">
                        Privacy Policy
                    </h1>







                </div>
                <div class="column is-9 is-offset-1">

                </div>

            </div>
        </div>
    </div>
    <!-- /Hero image -->
    </div>
    <!-- /Hero and nav -->

    <section class="section is-medium">
        <div class="container">
            <!-- Title -->
            <div class="section-title-wrapper">
                <h2 class="title dark-text text-bold main-title is-2 has-text-centered">
                    Privacy Policy
                </h2>

            </div>
            <!-- /Title -->
            <div class="columns">
                <div class="column is-6 is-offset-3">
                    <div class="content">

                        <div class="text-widget">
                            <div class="text-widget">
                                <h2>Disclaimer Policy</h2>
                                <p>Information on this website may be subjected to inaccuracies or typographical errors. This website doesn't constitute an offer to contract. Tecions makes no representations whatsoever about other websites that endorse the same nature of intellectual services we offer. We exercise no exclusive control whatsoever over the content of foreign websites. We do not endorse or accept any responsibility for any other website except ours.<span>&nbsp;</span><br /><br />Without limiting the foregoing, Tecions shall not be liable to you or your business for any incidental, consequential, special, or punitive damages/lost or imputed profits/royalties arising out of specific agreements, goods or services provided - whether for breach of warranty or any other obligation arising therefrom or otherwise, whether liability is asserted in contract or tort(including negligence and strict product liability) and irrespective of whether we have been advised of the possibility of any such loss or damage. Each party hereby waives any claims that these exclusions deprive such party of an adequate remedy. You acknowledge that third party product and service providers may advertise their products and services on Tecions making any representation or warranty regarding any third party products and services to you. However, you acknowledge and agree that at no time is Tecions making any representation or warranty regarding third party product &amp; services, nor will Tecions be liable to you or any third party over claims arising from or in connection with such third party products and services. You hereby disclaim and waive any rights and claims you may have against Tecions with respect to third party products and services, to the maximum extent permitted by the law.</p>
                            </div>
                            <div class="text-widget">
                                <h2>Privacy Policy</h2>
                                <p>We at Tecions, hold your Privacy to the highest esteem. We strictly discourage &amp; refrain from giving, leasing or selling or simply disclosing personal client information like contact forms, download requests, email lists or simple general or contact information.<span>&nbsp;</span><br /><br />Any information you submit to our official website will be held at the state of utmost confidentiality. For further queries, please email us.</p>
                            </div>
                        </div>
                </div>


            </div>
        </div>
        </div>
    </section>



@endsection