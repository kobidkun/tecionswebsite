@extends('frontend.base')


@section('content')
    <title>Tecions - We Deliver High Productivity Web and Mobile Application</title>
<!-- Hero and nav -->


    <!-- Hero image -->
    <div id="main-hero" class="hero-body is-clean">
        <div class="container has-text-centered">
            <div class="columns is-vcentered">
                <div class="column is-5 caption-column has-text-left">
                    <h1 class="clean-title light-text">
                        Increase your Productivity
                    </h1>
                    <div class="subtitle is-5 no-margin">
                        We provide IT Solution to boost up your productivity, Sales and Workflow.
                    </div>
                    <div class="cta-wrapper has-text-left">
                        <a data-modal="horizontal-form-modal" class="modal-trigger button button-cta btn-align btn-outlined is-bold light-btn"

                        >
                            Schedule Meeting
                        </a>

                    </div>






                </div>
                <div class="column is-9 is-offset-1">
                    <figure class="image is-3by2">
                        <img class="clean-hero-mockup mt-80 z-index-2"
                             src="{{asset('img/files/macbook-app.png')}}" alt="">
                    </figure>
                </div>

            </div>
        </div>
    </div>
    <!-- /Hero image -->
</div>
<!-- /Hero and nav -->

<!-- Product -->
<section id="product" class="section is-medium is-skewed-sm">
    <div class="container is-reverse-skewed-sm">
        <!-- Title -->
        <div class="section-title-wrapper has-text-centered">
            <div class="clean-bg-symbol"><i class="fa fa-gg"></i></div>
            <div>
                <h2 class="clean-section-title">Our Core Services</h2>
                <h3 class="subtitle is-5 pt-10 pb-10">
                    Let make most out of your business together
                </h3>
            </div>
        </div>

        <div class="content-wrapper">
            <!-- Icon boxes -->
            <div class="columns is-vcentered">
                <div class="column is-5 is-offset-1 has-text-centered">
                    <div class="columns is-vcentered has-text-centered is-multiline">
                        <!-- Icon box -->
                        <div class="column is-6">
                            <div class="flex-card icon-card light-bordered hover-inset padding-20">
                                <img src="{{asset('img/services/002-application.png')}}" alt="">
                                <div class="icon-card-text is-clean mt-10">
                                    Application Development
                                </div>
                            </div>
                        </div>
                        <!-- Icon box -->
                        <div class="column is-6">
                            <div class="flex-card icon-card light-bordered hover-inset padding-20">
                                <img src="{{asset('img/services/003-cloud-computing.png')}}" alt="">
                                <div class="icon-card-text is-clean mt-10">
                                    Cloud Computing
                                </div>
                            </div>
                        </div>
                        <!-- Icon box -->
                        <div class="column is-6">
                            <div class="flex-card icon-card light-bordered hover-inset padding-20">
                                <img src="{{asset('img/services/004-menu.png')}}" alt="">
                                <div class="icon-card-text is-clean mt-10">
                                    UI / UX Design
                                </div>
                            </div>
                        </div>
                        <!-- Icon box -->
                        <div class="column is-6">
                            <div class="flex-card icon-card light-bordered hover-inset padding-20">
                                <img src="{{asset('img/services/001-smartphone.png')}}" alt="">
                                <div class="icon-card-text is-clean mt-10">
                                    Mobility
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Feature text -->
                <div class="column is-5 is-offset-1">
                    <div class="content padding-20">
                        <h2 class="feature-headline is-clean">Providing Solution with most advanced Technology</h2>
                        <p class="no-margin-bottom pt-5 pb-5">
                            What makes us unique from others?
                        </p>
                        <p class="no-margin-bottom pt-5 pb-5">
                            We develop Products with most  advanced technology available.
                            This making your business more efficient and productive.
                        </p>
                        <p class="no-margin-bottom pt-5 pb-5">
                            Lets develop an amazing product together.
                        </p>
                        <div class="pt-10 pb-10">
                            <a data-modal="horizontal-form-modal"  class=" modal-trigger button button-cta btn-align raised primary-btn">
                                Contact us
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Icon boxes -->
        </div>
    </div>
</section>
<!-- /Product -->

<!-- Services -->
{{--<section id="services" class="section section-feature-grey is-medium is-skewed-sm">
    <div class="container is-reverse-skewed-sm">
        <!-- Title -->
        <div class="section-title-wrapper has-text-centered">
            <div class="clean-bg-symbol"><i class="fa fa-gg"></i></div>
            <div>
                <h2 class="clean-section-title">Integrate your Process.</h2>
                <h3 class="subtitle is-5 pt-10 pb-10">
                    Access integrations and new features in a matter of seconds
                </h3>
            </div>
        </div>

        <div class="content-wrapper">
            <!-- Hover boxes -->
            <div class="columns is-vcentered">
                <div class="column"></div>
                <div class="column is-10">
                    <div class="columns is-vcentered">
                        <div class="column is-6">
                            <!-- Hover box -->
                            <div class="flex-card icon-card-hover first-card light-bordered padding-20">
                                <h3 class="card-title is-clean">
                                    React Faster
                                </h3>
                                <p class="card-description">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte.</p>
                            </div>
                            <!-- Hover box -->
                            <div class="flex-card icon-card-hover second-card light-bordered padding-20">
                                <h3 class="card-title is-clean">
                                    Learn from data
                                </h3>
                                <p class="card-description">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte.</p>
                            </div>
                        </div>
                        <div class="column is-6">
                            <!-- Hover box -->
                            <div class="flex-card icon-card-hover third-card light-bordered padding-20">
                                <h3 class="card-title is-clean">
                                    Setup workflows
                                </h3>
                                <p class="card-description">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte.</p>
                            </div>
                            <!-- Hover box -->
                            <div class="flex-card icon-card-hover fourth-card light-bordered padding-20">
                                <h3 class="card-title is-clean">
                                    Scale up
                                </h3>
                                <p class="card-description">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column"></div>
            </div>
            <!-- /Hover boxes -->
            <div class="has-text-centered is-title-reveal pt-40 pb-40">
                <a href="landing-v1-signup.html" class="button button-cta btn-align raised primary-btn raised">
                    Start your Free Trial
                </a>
            </div>
        </div>
    </div>
</section>--}}
<!-- /Services -->

<!-- Features -->
{{--<section id="features" class="section is-medium is-skewed-sm">
    <div class="container is-reverse-skewed-sm">
        <!-- Title -->
        <div class="section-title-wrapper has-text-centered">
            <div class="clean-bg-symbol"><i class="fa fa-gg"></i></div>
            <div>
                <h2 class="clean-section-title">Key features.</h2>
                <h3 class="subtitle is-5 pt-10 pb-10">
                    Access integrations and new features in a matter of seconds
                </h3>
            </div>
        </div>

        <div class="content-wrapper">
            <!-- Navigation pills -->
            <div class="columns is-vcentered">
                <div class="column">
                    <div class="navigation-tabs outlined-pills animated-tabs mb-40">
                        <div class="tabs is-centered">
                            <ul>
                                <li class="tab-link is-active" data-tab="new-deals"><a>Customers</a></li>
                                <li class="tab-link" data-tab="invoices"><a>Invoices</a></li>
                                <li class="tab-link" data-tab="reporting"><a>Reporting</a></li>
                            </ul>
                        </div>
                        <!-- Pill content -->
                        <div id="new-deals" class="navtab-content pt-20 pb-20 is-active">
                            <div class="columns is-vcentered">
                                <div class="column is-6">
                                    <figure class="image">
                                        <img src="assets/images/illustrations/mockups/landing1/opportunities.png" alt="">
                                    </figure>
                                </div>
                                <div class="column is-6">
                                    <div class="content padding-20">
                                        <h2 class="feature-headline is-clean">Create an incredible user and customer experience</h2>
                                        <p class="no-margin-bottom pt-5 pb-5">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. </p>
                                        <p class="no-margin-bottom pt-5 pb-5">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. Vis mutat altera percipit ad, ipsum prompta ius eu. Sanctus appellantur vim ea. Dolorem delicata vis te, aperiam nostrum ut per.</p>
                                        <div class="pb-10 pt-10">
                                            <a href="landing-v1-features.html" class="button btn-align btn-more is-link color-primary">
                                                Learn more about deals <i class="sl sl-icon-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pill content -->
                        <div id="invoices" class="navtab-content pt-20 pb-20">
                            <div class="columns is-vcentered">
                                <div class="column is-6">
                                    <figure class="image">
                                        <img src="assets/images/illustrations/mockups/landing1/invoices.png" alt>
                                    </figure>
                                </div>
                                <div class="column is-6">
                                    <div class="content padding-20">
                                        <h2 class="feature-headline is-clean">Create invoices seamlessly and get paid incredibly fast</h2>
                                        <p class="no-margin-bottom pt-5 pb-5">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. </p>
                                        <p class="no-margin-bottom pt-5 pb-5">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. Vis mutat altera percipit ad, ipsum prompta ius eu. Sanctus appellantur vim ea. Dolorem delicata vis te, aperiam nostrum ut per.</p>
                                        <div class="pt-10 pb-10">
                                            <a href="landing-v1-features.html" class="button btn-align btn-more is-link color-primary">
                                                Learn more about invoices <i class="sl sl-icon-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pill content -->
                        <div id="reporting" class="navtab-content pt-20 pb-20">
                            <div class="columns is-vcentered">
                                <div class="column is-6">
                                    <figure class="image">
                                        <img src="assets/images/illustrations/mockups/landing1/reports.png" alt="">
                                    </figure>
                                </div>
                                <div class="column is-6">
                                    <div class="content padding-20">
                                        <h2 class="feature-headline is-clean">Get insight on your data and take managed decisions</h2>
                                        <p class="no-margin-bottom pt-5 pb-5">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. </p>
                                        <p class="no-margin-bottom pt-5 pb-5">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. Vis mutat altera percipit ad, ipsum prompta ius eu. Sanctus appellantur vim ea. Dolorem delicata vis te, aperiam nostrum ut per.</p>
                                        <div class="pt-10 pb-10">
                                            <a href="landing-v1-signup.html" class="button btn-align btn-more is-link color-primary">
                                                Learn more about reporting <i class="sl sl-icon-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Navigation pills -->
        </div>
    </div>
</section>--}}
<!-- /Features -->

<!-- Slanted feature Section -->
{{--<section class="section section-primary is-medium is-skewed-sm is-relative">
    <div class="container slanted-container is-reverse-skewed-sm pt-40 pb-40">
        <div class="columns is-vcentered">
            <!-- Content -->
            <div class="column is-5 ">
                <div class="content padding-20">
                    <h2 class="title is-3 clean-text light-text no-margin-bottom">Get started in a breeze</h2>
                    <p class="light-text no-margin-bottom pt-10 pb-10">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. Vis mutat altera percipit ad, ipsum prompta ius eu. Sanctus appellantur vim ea. Dolorem delicata vis te, aperiam nostrum ut per.</p>
                    <div class="pt-10 pb-10">
                        <a href="landing-v1-signup.html" class="button button-cta light-btn btn-outlined is-bold">
                            Get started
                        </a>
                    </div>
                </div>
            </div>
            <!-- Featured image -->
            <div class="column is-6 is-offset-1">
                <img class="featured-svg" src="assets/images/illustrations/mockups/landing1/taxes.png" alt="">
            </div>
        </div>
    </div>
</section>--}}
<!-- /Slanted feature Section -->

<!-- Values -->
{{--<section id="values" class="section section-feature-grey is-medium is-skewed-sm">
    <div class="container is-reverse-skewed-sm">
        <div class="section-title-wrapper has-text-centered">
            <div class="clean-bg-symbol"><i class="fa fa-gg"></i></div>
            <div>
                <h2 class="clean-section-title">Grow your Business.</h2>
                <h3 class="subtitle is-5 pt-10">
                    Access integrations and new features in a matter of seconds
                </h3>
            </div>
        </div>

        <div class="content-wrapper">
            <div class="columns is-vcentered">
                <!-- Floating icon -->
                <div class="column is-4">
                    <div class="floating-circle levitate is-icon-reveal">
                        <img src="assets/images/illustrations/icons/landing-v1/globe-pointer.svg" alt="">
                    </div>
                    <div class="has-text-centered mt-20">
                        <span class="clean-text">Sell everywhere</span>
                    </div>
                </div>
                <!-- Floating icon -->
                <div class="column is-4">
                    <div class="floating-circle levitate delay-2 is-icon-reveal">
                        <img src="assets/images/illustrations/icons/landing-v1/cash.svg" alt="">
                    </div>
                    <div class="has-text-centered mt-20">
                        <span class="clean-text">Smart pricing</span>
                    </div>
                </div>
                <!-- Floating icon -->
                <div class="column is-4">
                    <div class="floating-circle levitate delay-4 is-icon-reveal">
                        <img src="assets/images/illustrations/icons/landing-v1/credit-card.svg" alt="">
                    </div>
                    <div class="has-text-centered mt-20">
                        <span class="clean-text">Secured payments</span>
                    </div>
                </div>
            </div>
            <!-- CTA -->
            <div class="has-text-centered is-title-reveal pt-40 pb-40">
                <a href="landing-v1-signup.html" class="button button-cta btn-align raised primary-btn">
                    Start your Free trial
                </a>
            </div>
        </div>
    </div>
</section>--}}
<!-- /Values -->

<!-- Clients -->
{{--<section id="business-types" class="section is-medium is-skewed-sm">
    <div class="container is-reverse-skewed-sm">

        <div class="section-title-wrapper has-text-centered">
            <div class="clean-bg-symbol"><i class="fa fa-gg"></i></div>
            <div>
                <h2 class="clean-section-title">We got you covered</h2>
                <h3 class="subtitle is-5 pt-10">
                    Every business matters, learn how we handle it.
                </h3>
            </div>
        </div>

        <!-- Content -->
        <div class="content-wrapper">
            <div class="columns is-vcentered">
                <div class="column is-5 is-offset-1">
                    <div class="content padding-20">
                        <h2 class="feature-headline is-clean no-margin-bottom">Every business matters. We give you tools to succeed.</h2>
                        <p class="no-margin-bottom pt-5 pb-5">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. </p>
                        <p class="no-margin-bottom pt-5 pb-5">Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. Vis mutat altera percipit ad, ipsum prompta ius eu. Sanctus appellantur vim ea. </p>
                        <div class="pb-10 pt-10">
                            <a href="landing-v1-signup.html" class="button button-cta btn-align raised primary-btn">
                                Try it free
                            </a>
                        </div>
                    </div>
                </div>
                <!-- Card with icons -->
                <div class="column is-4 is-offset-1">
                    <div class="flex-card raised padding-20">
                        <div class="icon-group">
                            <img src="assets/images/illustrations/icons/landing-v1/store.svg" alt="">
                            <span>Online stores</span>
                        </div>
                        <div class="icon-group">
                            <img src="assets/images/illustrations/icons/landing-v1/bank.svg" alt="">
                            <span>Finance services</span>
                        </div>
                        <div class="icon-group">
                            <img src="assets/images/illustrations/icons/landing-v1/factory.svg" alt="">
                            <span>Industry</span>
                        </div>
                        <div class="icon-group">
                            <img src="assets/images/illustrations/icons/landing-v1/church.svg" alt="">
                            <span>Churches</span>
                        </div>
                        <div class="icon-group">
                            <img src="assets/images/illustrations/icons/landing-v1/warehouse.svg" alt="">
                            <span>Logistics</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>--}}
<!-- /Clients -->

<!-- Testimonials section -->
{{--<section id="card-testimonials" class="section section-feature-grey is-medium is-skewed-sm">
    <div class="container is-reverse-skewed-sm">
        <!-- Title -->
        <div class="section-title-wrapper has-text-centered">
            <div class="clean-bg-symbol"><i class="fa fa-gg"></i></div>
            <div>
                <h2 class="clean-section-title">We are Trusted.</h2>
                <h3 class="subtitle is-5 pt-10">
                    Access integrations and new features in a matter of seconds
                </h3>
            </div>
        </div>

        <div class="content-wrapper">
            <!-- Testimonials -->
            <div class="columns">
                <div class="column is-hidden-mobile"></div>
                <div class="column is-10">
                    <div class="columns is-vcentered">
                        <div class="column is-6">
                            <!-- Testimonial item -->
                            <div class="flex-card testimonial-card light-raised padding-20">
                                <div class="testimonial-title">
                                    Amazed by the product
                                </div>
                                <div class="testimonial-text">
                                    Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. Vis mutat altera percipit ad.
                                </div>
                                <div class="user-id">
                                    <img class="" src="assets/images/avatars/dan.png" alt="">
                                    <div class="info">
                                        <div class="name clean-text">Dan Shwartz</div>
                                        <div class="position">Software engineer</div>
                                    </div>
                                </div>
                            </div>
                            <!-- Testimonial item -->
                            <div class="flex-card testimonial-card light-raised padding-20">
                                <div class="testimonial-title">
                                    My tasks are now painless
                                </div>
                                <div class="testimonial-text">
                                    Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. Vis mutat altera percipit ad.
                                </div>
                                <div class="user-id">
                                    <img class="" src="assets/images/avatars/jane.jpg" alt="">
                                    <div class="info">
                                        <div class="name clean-text">Jane Guzmann</div>
                                        <div class="position">CFO</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column is-6">
                            <!-- Testimonial item -->
                            <div class="flex-card testimonial-card light-raised padding-20">
                                <div class="testimonial-title">
                                    Very nice support
                                </div>
                                <div class="testimonial-text">
                                    Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. Vis mutat altera percipit ad.
                                </div>
                                <div class="user-id">
                                    <img class="" src="assets/images/avatars/helen.jpg" alt="">
                                    <div class="info">
                                        <div class="name clean-text">Hellen Miller</div>
                                        <div class="position">Accountant</div>
                                    </div>
                                </div>
                            </div>
                            <!-- Testimonial item -->
                            <div class="flex-card testimonial-card light-raised padding-20">
                                <div class="testimonial-title">
                                    My income has doubled
                                </div>
                                <div class="testimonial-text">
                                    Lorem ipsum dolor sit amet, vim quidam blandit voluptaria no, has eu lorem convenire incorrupte. Vis mutat altera percipit ad.
                                </div>
                                <div class="user-id">
                                    <img class="" src="assets/images/avatars/anthony.jpg" alt="">
                                    <div class="info">
                                        <div class="name clean-text">Anthony Leblanc</div>
                                        <div class="position">Founder at Hereby</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-hidden-mobile"></div>
            </div>
            <!-- /Testimonials -->

            <!-- Clients grid -->
            <div class="grid-clients three-grid pt-80 pb-80">
                <div class="columns is-vcentered">
                    <div class="column"></div>
                    <div class="column">
                        <a><img class="client" src="assets/images/logos/custom/systek.svg" alt=""></a>
                    </div>
                    <div class="column">
                        <a><img class="client" src="assets/images/logos/custom/phasekit.svg" alt=""></a>
                    </div>
                    <div class="column">
                        <a><img class="client" src="assets/images/logos/custom/grubspot.svg" alt=""></a>
                    </div>
                    <div class="column"></div>
                </div>
                <div class="columns is-vcentered is-separator">
                    <div class="column"></div>
                    <div class="column">
                        <a><img class="client" src="assets/images/logos/custom/tribe.svg" alt=""></a>
                    </div>
                    <div class="column">
                        <a><img class="client" src="assets/images/logos/custom/kromo.svg" alt=""></a>
                    </div>
                    <div class="column">
                        <a><img class="client" src="assets/images/logos/custom/covenant.svg" alt=""></a>
                    </div>
                    <div class="column"></div>
                </div>
                <div class="columns is-vcentered is-separator">
                    <div class="column"></div>
                    <div class="column">
                        <a><img class="client" src="assets/images/logos/custom/infinite.svg" alt=""></a>
                    </div>
                    <div class="column">
                        <a><img class="client" src="assets/images/logos/custom/gutwork.svg" alt=""></a>
                    </div>
                    <div class="column">
                        <a><img class="client" src="assets/images/logos/custom/proactive.svg" alt=""></a>
                    </div>
                    <div class="column"></div>
                </div>
            </div>
            <!-- /Clients grid -->
        </div>
    </div>
</section>--}}
<!-- /Testimonials section -->





<!-- /Modal trigger -->

<!-- Modal Markup -->
<style>
    .modal.is-active .modal-background{
        background: -webkit-linear-gradient(45deg, #e94754 0, #e94754 55%)
    }

    .modal-background{
        background: -webkit-linear-gradient(45deg, #e94754 0, #c13a45 55%)
    }
</style>
<div id="horizontal-form-modal" class="modal modal-lg">
    <div class="modal-background"></div>
    <div class="modal-content">
        <div class="flex-card simple-shadow">
            <div class="card-body">
                <form method="POST" action="{{ route('form.save') }}" >
                    {{ csrf_field() }}
                    <div class="columns mt-40">
                        <div class="column">
                            <div class="control">
                                <label>Name</label>
                                <input name="name" class="input is-medium mt-5" type="text" required>
                            </div>
                            <div class="control">
                                <label>Mobile Number</label>
                                <input name="mobile" class="input is-medium mt-5" type="tel" required>
                            </div>
                        </div>
                        <div class="column">
                            <div class="control">
                                <label>Email Id</label>
                                <input name="email" class="input is-medium mt-5" type="email" required>
                            </div>
                            <div class="control">
                                <label>City</label>
                                <input  name="city" class="input is-medium mt-5" type="text" required>
                            </div>
                        </div>
                    </div>

                    <div class="mt-10">
                        <button type="submit" class="button btn-align no-lh raised primary-btn">Submit</button>
                        <button class="button is-link no-lh modal-dismiss">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <button class="modal-close is-large is-hidden" aria-label="close"></button>
</div>
<!-- /Modal Markup -->






<!-- CTA -->
<section id="cta" class="section is-medium is-skewed-sm">
    <div class="container is-reverse-skewed-sm">
        <div class="section-title-wrapper">
            <div class="clean-bg-symbol"><i class="fa fa-gg"></i></div>
            <div>
                <h2 class="clean-section-title has-text-centered">Let's dive in.</h2>
            </div>
        </div>
        <div class="content">
            <h4 class="has-text-centered">Lets Discuss about your Project</h4>
        </div>
        <div class="content-wrapper giant-pb">
            <form method="POST" action="{{ route('form.save') }}">
                {{ csrf_field() }}
                <div class="columns">
                    <div class="column is-8 is-offset-2">
                        <div class="columns is-vcentered">
                            <div class="column is-3">
                                <!-- Form field -->
                                <div class="control-material is-accent">
                                    <input class="material-input" name="name" type="text" required>
                                    <span class="material-highlight"></span>
                                    <span class="bar"></span>
                                    <label>Name *</label>
                                </div>
                                <!-- /Form field -->
                            </div>
                            <div class="column is-3">
                                <!-- /Form field -->
                                <div class="control-material is-accent">
                                    <input class="material-input" name="email" type="text" required>
                                    <span class="material-highlight"></span>
                                    <span class="bar"></span>
                                    <label>Email *</label>
                                </div>
                                <!-- /Form field -->
                            </div>
                            <div class="column is-3">
                                <!-- Form field -->
                                <div class="control-material is-accent">
                                    <input class="material-input" name="mobile" type="text" required>
                                    <span class="material-highlight"></span>
                                    <span class="bar"></span>
                                    <label>Mobile *</label>
                                </div>
                                <!-- /Form field -->
                            </div>

                            <div class="column is-3">
                                <!-- Form field -->
                                <div class="control-material is-accent">
                                    <input class="material-input" type="text" name="city" required>
                                    <span class="material-highlight"></span>
                                    <span class="bar"></span>
                                    <label>City *</label>
                                </div>
                                <!-- /Form field -->
                            </div>
                            <div class="column is-3">
                                <button type="submit" class="button button-cta btn-align
                                secondary-btn btn-outlined is-bold rounded raised no-lh">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- /CTA -->

    @endsection