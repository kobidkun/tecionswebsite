@extends('frontend.base')


@section('content')
    <title>Tecions - Terms & Conditions</title>
    <!-- Hero and nav -->


    <!-- Hero image -->
    <div id="main-hero" class="hero-body is-clean">
        <div class="container has-text-centered">
            <div class="columns is-vcentered">
                <div class="column is-5 caption-column has-text-left">
                    <h1 class="clean-title light-text">
                        Terms & Conditions
                    </h1>







                </div>
                <div class="column is-9 is-offset-1">

                </div>

            </div>
        </div>
    </div>
    <!-- /Hero image -->
    </div>
    <!-- /Hero and nav -->

    <section class="section is-medium">
        <div class="container">
            <!-- Title -->
            <div class="section-title-wrapper">
                <h2 class="title dark-text text-bold main-title is-2 has-text-centered">
                    Terms & Conditions
                </h2>

            </div>
            <!-- /Title -->
            <div class="columns">
                <div class="column is-6 is-offset-3">
                    <div class="content">

                        <p>

                            <div class="text-widget">
                        <p>This page contains the Terms and Conditions governing your use of the tecions.com / kunda dsales private limited web Site and its services. Please read this page carefully. By submitting the enquiry form or by hiring any resource from Tecions, the client agrees to accept the following terms and conditions.<span>&nbsp;</span><br /><br />Please read these Terms and Conditions carefully. If you do not accept these Terms and Conditions, you may not use this service.<span>&nbsp;</span><br /><br />Tecions provides individual remote workers including remote designer ("Resources"):</p>
                    </div>
                    <hr />
                    <div class="text-widget">
                        <h3>This Service Also Includes</h3>
                        <ul class="tncu">
                            <li>Screening, Hiring, contracting and payment of Resources,</li>
                            <li>Assignment of Resources to Customer projects</li>
                            <li>Time log tracking and reporting,</li>
                            <li>Software tools including Tecions Team and Tecions Share ("Tools"), and</li>
                            <li>Any initial customer training and project support.</li>
                            <li>The assignment of Resources, including work timings and start date will be agreed upon by both parties and documented in an email to Customer.</li>
                            <li>Tecions will provide initial training, including instructions on how to communicate better, an introduction to Resource(s).</li>
                            <li>Customer understands that each Resource will be a full time employee of Tecions. Customer will work directly with Resource and Customer will be solely responsible for the projects, and performance of any work product ("Work Product") developed by Resources for Customer.</li>
                            <li>Customer may terminate assigned Resource at any time during the by providing a minimum 15 days notice for each Resource that needs to be terminated after the Trial period is over</li>
                            <li>Customer will provide licenses to any 3rd Party software that the resource may require to complete the assigned task provided such software(s) are not already available with Tecions.</li>
                            <li>Customer will be responsible for incidental expenses, mailing fees, travel expenses, and any other fees that may be incurred by Tecions or Resource on behalf of Customer in connection with this Agreement ("Project Expenses"), provided Customer gave prior approval for these expenses.</li>
                            <li>Tecions will allocate sufficient Senior PHP Developers for the Customer.</li>
                        </ul>
                    </div>
                    <div class="text-widget">
                        <h3>Fees and Payments</h3>
                        <ul class="tncu">
                            <li>Tecions will email Customer with and invoice and time logs detailing hours and work done by each Resource. Any corrections to hours worked must be received in writing within three days and any adjustments will be reflected in the following invoice.</li>
                            <li>Tecions may change terms of Resource Assignment, including rates, with 60 days written notice to Customer.</li>
                            <li>The client must pay idle time of the resource. Tecions will not responsible for any idle time in case client has not task to be allocated to the resource(s)</li>
                            <li>Tecions will proportionately lower the fees if the number if days worked is less then the specified output.</li>
                        </ul>
                    </div>
                    <div class="text-widget">
                        <h3>Term and Termination</h3>
                        <ul class="tncu">
                            <li>Customer may terminate this Agreement at any time provided all Resource assignments have been terminated and any outstanding account balance has been paid in full.</li>
                            <li>Tecions may terminate this Agreement or any part of the Tecions services</li>
                            <li>at any time in the event Customer defaults on any obligation hereunder, including non-payment of fees, and does not remedy such default within ten (7) days of notice thereof or</li>
                            <li>Upon thirty (30) days written notice if Tecions terminates or significantly alters its product or service offering.</li>
                            <li>Effect of Termination: Tecions will cease charging Customer for any new Service Fees after termination of this Agreement. Unless otherwise specified in writing by Tecions, Customer will not receive any refund for payments already made by Customer. If termination of this Agreement is due to Customer default hereunder, Customer shall bear all costs of such termination, including any reasonable collection costs or costs that Tecions incurs in closing Customer account. Upon termination, Customer shall destroy any copy of the materials licensed to Customer hereunder. Customer agrees that upon termination or discontinuance for any reason, Tecions may delete all information related to Customer on the Tecions Tools, if applicable. In addition to the terms set forth herein, certain Tecions services may have additional terms regarding termination, which are set forth in the Resource Assignment Email.</li>
                        </ul>
                    </div>
                    <div class="text-widget">
                        <h3>Confidential and Proprietary Information</h3>
                        <ul class="tncu">
                            <li>Each party shall keep confidential and not disclose to any third party or use, except as required by this Agreement, non-public information obtained from the other party; provided, however, that neither party shall be prohibited from disclosing or using information,</li>
                            <li>That at the time of disclosure is publicly available or becomes publicly available through no act or omission of the party having a confidentiality obligation under this section,</li>
                            <li>That is or has been disclosed to such party by a third party who is not under (and to whom such party does not owe) an obligation of confidentiality with respect thereto,</li>
                            <li>That is or has been independently acquired or developed by such party, or</li>
                            <li>To the minimum extent possible, as required by court order or as otherwise required by law, on condition that notice of such requirement by law or judgment for such disclosure is given to the other party prior to making any such use or disclosure.</li>
                            <li>Tecions agrees that its employees and contractors working on Customer assignments have signed or will sign a non-disclosure agreement requiring at least the level of confidentiality specified above.</li>
                        </ul>
                    </div>
                    <div class="text-widget">
                        <h3>Ownership</h3>
                        <ul class="tncu">
                            <li>Customer shall retain ownership of all data, software applications, tools, other intellectual property, etc. ("Customer Materials") supplied for use under this Agreement. Customer warrants that it either owns or has a valid license to use or have used Customer Materials provided to Tecions for use in performing services for Customer and grants a license to use such Customer Materials. Tecions agrees to restrict the use of Customer Materials to employees and contractors performing services for Customer and to return all Customer Materials upon request or completion of the assignment.</li>
                            <li>Upon receipt of payment for the services provided, Customer shall own the software developed by Tecions for Customer under this Agreement ("Work Product"). Tecions agrees to perform, during and after performance of services, all acts deemed necessary or desirable by Customer, at Customer's expense based on Tecions' standard billing rates in effect at the time, to perfect and enforce the full benefits, enjoyment, rights and title throughout the world in the Work Product. Such acts may include, but are not limited to, execution of documents and assistance or cooperation in the registration and enforcement of applicable patents and copyrights or other legal proceedings. In the event that Customer is unable for any reason whatsoever to secure Tecions' or engineer's signature to any lawful and necessary document required to apply for or execute any patent, copyright or other applications with respect to any Work Product (including improvements, renewals, extensions, continuations, divisions or continuations in part thereof), Tecions hereby irrevocably appoints Customer and its duly authorized officers and agents as its agents and attorneys-in-fact to execute and file any such application and to do all other lawfully permitted acts to further the prosecution and issuance of patents, copyrights or other rights thereon with the same legal force and effect as if executed by Tecions.</li>
                            <li>Tecions agrees that its employees and contractors working on Customer assignments have signed or will sign an agreement requiring at least the level of assistance specified above and agreeing that ownership of such Work Product transfers to Customer.</li>
                            <li>Notwithstanding anything to the contrary in this Agreement, Tecions shall not be prohibited or enjoined at any time by Customer from utilizing any "skills or knowledge of a general nature" acquired during the course of performing the services specified under this Agreement. For purposes of this Agreement, "skills or knowledge of a general nature" shall include, without limitation, anything that might reasonably be learned or acquired on Customer assignment that could be used on similar work performed for other clients.</li>
                        </ul>
                    </div>
                    <div class="text-widget">
                        <h3>Indemnification</h3>
                        <p>Each party shall indemnify and hold harmless the other party (and its subsidiaries, affiliates, officers, agents, co-branders or other partners, and employees) from any and all claims, damages, liabilities, costs, and expenses (including, but not limited to, reasonable attorneys' fees and all related costs and expenses) incurred as a result of any claim, judgment, or adjudication arising from a claim that use of that party's software, or its use of third party software, infringes upon the intellectual property rights of a third party. To qualify for such defense and payment, the party must:</p>
                        <ul class="tncu">
                            <li>Give the other party prompt written notice of a claim; and</li>
                            <li>Allow that party to control, and fully cooperate with it, in the defense and all related negotiations.</li>
                        </ul>
                    </div>
                    <div class="text-widget">
                        <h3>Warranties and Disclaimers</h3>
                        <ul class="tncu">
                            <li>Tecions will use reasonable commercial efforts to provide the professional services as set forth in the Resource Assignment Email in accordance with this Agreement, and that the services will be performed in a workman like fashion. Customer understands that there may be occasions when an assigned engineer becomes unavailable either temporarily or permanently. Tecions's responsibility in such cases is to provide a comparable replacement Resource.</li>
                            <li>Limitation of Remedies: Tecions's entire liability and exclusive remedy in any cause of action based on contract, tort or otherwise in connection with any services furnished pursuant to this Agreement shall be limited to the total fees paid by Customer to Tecions. No action, regardless of form, arising out of this Agreement may be brought by either party more than one year after the occurrence of the event giving rise to such cause of action.</li>
                            <li>NEITHER Tecions NOR ANYONE ELSE WHO HAS BEEN INVOLVED IN THE CREATION, PRODUCTION, OR DELIVERY OF THE SERVICES SHALL IN ANY EVENT WHATSOEVER BE LIABLE FOR ANY INDIRECT, CONSEQUENTIAL, PUNITIVE OR INCIDENTAL DAMAGES (INCLUDING DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION, AND THE LIKE) ARISING OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE DEVELOPED FOR CLIENT EVEN IF Tecions HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</li>
                        </ul>
                    </div>
                    <div class="text-widget">
                        <h3>Severability</h3>
                        <p>Should any provision here of be deemed, for any reason whatsoever, to be invalid or inoperative, such provision shall be deemed severable and shall not affect the force and validity of other provisions of this Agreement.</p>
                    </div>
                    <div class="text-widget">
                        <h3>Entire Agreement</h3>
                        <p>This Agreement, including the Customer Account information and Resource Assignment Email(s), sets forth the entire understanding of the parties as to the subject matter therein and may not be modified except in a writing executed by both parties. Any notices in connection with this Agreement must be in writing and sent by first class mail or equivalent, confirmed facsimile or major commercial rapid delivery courier service for Customer to the address specified in the Customer Account or to Tecions at 40, Swamiji Sarani, Near Vivekananda School Hakimpara, Siliguri, West Bengal, 734001 or such other address as may be properly specified in a subsequent written notice.</p>
                    </div>
                    <div class="text-widget">
                        <h3>Independent Parties</h3>
                        <p>For all purposes under this Agreement, each party shall be and act as an independent contractor of the other and shall not bind nor attempt to bind the other to any contract. Tecions will be solely responsible for its income taxes in connection with this Agreement and Customer will be responsible for sales, use and similar taxes, if any.</p>
                    </div>
                    <div class="text-widget">
                        <h3>Non-Solicitation of Personnel</h3>
                        <p>During the term of this Agreement, and for a period of one (1) year thereafter, Customer will not directly or indirectly solicit the employees or contractors of Tecions without the prior written consent of Tecions.</p>
                    </div>
                    <div class="text-widget">
                        <h3>Assignment</h3>
                        <p>Neither party shall have the right to assign this Agreement to another party except that Tecions may assign its rights and obligations to a successor to substantially all its relevant assets or business.</p>
                    </div>
                    <div class="text-widget">
                        <h3>Governing Law</h3>
                        <p>This contract and any dispute arising hereunder shall be governed by the laws of India without regard to principles on conflicts of laws.<span>&nbsp;</span><br /><br />The parties irrevocably submit to the exclusive jurisdiction of the courts of India for the purpose of hearing and determining any suit, action or proceedings or settling any disputes arising out of or in connection with this agreement and for the purpose of enforcement or any judgment against their respective assets.<span>&nbsp;</span><br /><br />The Contracts (Right of Third Parties) Act 1999 shall not apply to this agreement and no person other than the parties to this agreement shall have any rights under it, nor shall it be enforceable under that Act by any person other than the parties to it.</p>
                    </div>
                    <div class="text-widget">
                        <h3>Agreement to Be Bound</h3>
                        <p>By applying for Tecions service(s) through Tecions online signup process or otherwise, or by using the service(s) provided by Tecions under this Agreement, Customer acknowledges that Customer has read and agrees to be bound by all terms and conditions of this Agreement and documents incorporated by reference.</p>
                    </div>
                    <div class="text-widget">
                        <h3>Force Majeure</h3>
                        <p>Neither party undertakes any responsibility if it is prevented from performing its obligation due to sickness, accident, death of its employees or Consultants or any other cause beyond the control of such party.</p>
                    </div>



                            </p>
                    </div>


                </div>
            </div>
        </div>
    </section>



@endsection